const noble = require('noble');

let busy = false;
let poweredOn = false;
let shouldStartScanning = false;

noble.on('stateChange', function(state) {
	if (state === 'poweredOn') {
		poweredOn = true;
		
		if (shouldStartScanning) {
			noble.startScanning([], true);
		}
	}
});

function startScanning() {
	if (poweredOn) {
		noble.startScanning([], true);
	}
	else {
		shouldStartScanning = true;
	}
}

module.exports = {
	isNearby: function(address, callback, timeout) {
		if (busy) {
			callback(null);
			return;
		}
		
		busy = true;
		let timer = null;
		
		function returnStatus(result) {
			if (!busy) {
				return;
			}
			
			if (timer !== null) {
				clearTimeout(timer);
			}
			
			noble.removeListener('discover', onDiscover);
			noble.stopScanning();
			busy = false;
			callback(result);
		}

		function onDiscover(peripheral) {
			if (peripheral.address === address) {
				returnStatus(peripheral);
			}
		}
		
		noble.on('discover', onDiscover);
		
		timer = setTimeout(function() {
			timer = null;
			returnStatus(false);
		}, timeout || 10000);
		
		startScanning();
	}
};
