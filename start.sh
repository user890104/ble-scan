#!/bin/bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

DIR=/home/chip/ble-scan
cd $DIR
screen -dmS ble-scan node $DIR/server.js
