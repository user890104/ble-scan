if (typeof process.argv[2] !== 'string') {
	process.exit(2);
}

const scanner = require('./scanner');

scanner.isNearby(process.argv[2], function(result) {
	const found = (typeof result === 'object');
	
	if (found) {
		console.log('found');
	}
	else {
		if (result === null) {
			console.log('busy');
		}
		
		if (result === false) {
			console.log('not found');
		}
	}
	
	process.exit(found ? 0 : 1);
});
