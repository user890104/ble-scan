const fs = require('fs');
const http = require('http');
const HttpDispatcher = require('httpdispatcher');

const scanner = require('./scanner');

function leadingZero(num) {
	if (num > 9) {
		return num;
	}
	
	return '0' + num;
}

function logger(line) {
	const dt = new Date;
	const args = [line];
	
	args.unshift(
		'[' +
		leadingZero(dt.getDate()) + '.' +
		leadingZero(dt.getMonth()) + '.' +
		(dt.getFullYear()) + ' ' +
		leadingZero(dt.getHours()) + ':' +
		leadingZero(dt.getMinutes()) + ':' +
		leadingZero(dt.getSeconds()) +
		']'
	);
	
	console.log.apply(console, args);
}

const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
const dispatcher = new HttpDispatcher;

dispatcher.onGet('/find', function(req, res) {
	if (!('address' in req.params)) {
		res.writeHead(400, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			'error': 'missing_address'
		}));
		
		return;
	}
	
	scanner.isNearby(req.params.address, function(result) {
		if (result === null) {
			res.writeHead(503, {
				'Content-Type': 'application/json'
			});

			res.end(JSON.stringify({
				'error': 'busy'
			}));
			
			return;
		}
		
		const found = (typeof result === 'object');
		
		res.writeHead(200, {
			'Content-Type': 'application/json'
		});
		
		let obj = {
			'found': found
		};
		
		if (found) {
			obj.device = {
				id: result.id,
				uuid: result.uuid,
				address: result.address,
				addressType: result.addressType,
				connectable: result.connectable,
				advertisement: result.advertisement,
				rssi: result.rssi,
				services: result.services,
				state: result.state,
			};
		}

		res.end(JSON.stringify(obj));
	}, config.discovery && config.discovery.timeout);
});

http.createServer(function(req, res) {
	const conn = req.connection;
	
	logger('HTTP client connected: ' + conn.remoteAddress + ':' + conn.remotePort);
	logger(req.method + ' ' + req.url);
	
	try {
		dispatcher.dispatch(req, res);
	}
	catch(err) {
		logger(err);
	}
}).listen(config.listen.port, config.listen.hostname, function() {
	logger('Server listening on: http://' + config.listen.hostname + ':' + config.listen.port);
});
